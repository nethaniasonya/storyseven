$("input").click(() => {
    let body = document.querySelector('body')
    if (body.classList.contains('morning')) {
        body.classList.remove('morning');
        body.classList.add('sunset');
        document.getElementById("greeting").innerHTML = "enjoy the sunset:)";
    }
    else {
        body.classList.remove('sunset');
        body.classList.add('morning');
        document.getElementById("greeting").innerHTML = "good morning!";
    }
});
